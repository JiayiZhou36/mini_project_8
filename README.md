# Cloud Project 8
[![pipeline status](https://gitlab.com/JiayiZhou36/mini_project_8/badges/main/pipeline.svg)](https://gitlab.com/JiayiZhou36/mini_project_8/-/commits/main)

## Purpose of Project
This project creates a Rust command-Line tool with testing which load Goose dataset and does ETL-Query: Extract a dataset from URL, Transform, Load into SQLite Database, and Query.

## Requirements
* Rust command-line tool
* Data ingestion/processing--Goose.db, Goose.csv
* Unit tests

## Pipeline
![Screenshot_2024-03-31_at_9.25.34_PM](/uploads/b3d29d5be1187b06b250d76cdad28294/Screenshot_2024-03-31_at_9.25.34_PM.png)

## Test Report
![Screenshot_2024-03-31_at_9.16.35_PM](/uploads/0cb0cafdf9bb65c6955c02f89035d4ec/Screenshot_2024-03-31_at_9.16.35_PM.png)

##### Check Format and Test Erros: 
1. Format code `make format`
2. Lint code `make lint`
3. Test coce `make test`