use mini_project_8::{extract, query, transform_load};
use std::env;
use std::time::Instant;

fn main() {
    let args: Vec<String> = env::args().collect();

    let start_time = Instant::now();
    let mem_info_before: sys_info::MemInfo = sys_info::mem_info().unwrap();

    if args.len() < 2 {
        println!("Usage: {} [action]", args[0]);
        return;
    }

    let action = &args[1];
    match action.as_str() {
        "extract" => {
            extract(
                "https://raw.githubusercontent.com/fivethirtyeight/data/master/goose/goose_rawdata.csv?raw=true",
                "data/Goose.csv",
                "data",
            );
            let end_time = Instant::now();
            let _elapsed_time = end_time.duration_since(start_time);
            let _duration = start_time.elapsed(); // Prefixing with underscore to suppress warning

            let mem_info_after = sys_info::mem_info().unwrap();
            let _mem_used = mem_info_after.total - mem_info_before.total; // Prefixing with underscore to suppress warning
        }
        "transform_load" => match transform_load("data/Goose.csv") {
            Ok(_) => println!("Data loaded successfully!"),
            Err(err) => eprintln!("Error: {:?}", err),
        },
        "query" => {
            if let Some(q) = args.get(2) {
                if let Err(err) = query(q) {
                    eprintln!("Error: {:?}", err);
                } else {
                    println!("Query executed successfully!");
                }
            } else {
                println!("Usage: {} query [SQL query]", args[0]);
            }
        }
        _ => {
            println!("Invalid action. Use 'extract', 'transform_load', or 'query'.");
        }
    }
}

#[test]
fn test_extract() {
    let url =
        "https://raw.githubusercontent.com/fivethirtyeight/data/master/goose/goose_rawdata.csv?raw=true";
    let file_path = "data/Goose.csv";
    let directory = "data";

    extract(url, file_path, directory);

    assert!(std::fs::metadata(file_path).is_ok());
}

#[test]
fn test_transform_load() {
    let dataset = "data/Goose.csv";
    let result = transform_load(dataset);

    assert_eq!(result.unwrap(), "Goose.db");
}
