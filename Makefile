rust-version:
	@echo "Rust command-line utility versions:"
	rustc --version 			#rust compiler
	cargo --version 			#rust package manager
	rustfmt --version			#rust code formatter
	rustup --version			#rust toolchain manager
	clippy-driver --version		#rust linter

format:
	cargo fmt --quiet

install:
	# Install if needed
	#@echo "Updating rust toolchain"
	#rustup update stable
	#rustup default stable 

lint:
	cargo clippy --quiet

build:
	cargo build --release

test:
	cargo test -- --show-output >> test.md

extract: 
	cargo run extract

transform_load:
	cargo run transform_load

# Example: Create a database entry
create:
	cargo run query "INSERT INTO Goose (name, year, team, league, goose_eggs, broken_eggs, mehs, league_average_gpct, ppf, replacement_gpct, gwar, key_retro) VALUES ('Emma Watson', 1931, 'BSN', 'NL', 1, 0, 1, 0.67842, 99.0, 0.7342619, 0.1699602, 'watse101');"

# Example: Read from the database
read:
	cargo run query "SELECT * FROM Goose WHERE name='Emma Watson';"

# Example: Update a database entry
update:
	cargo run query "UPDATE Goose SET name='Emma Watson', year=1931, team='BSN', league='NL', goose_eggs=1, broken_eggs=0, mehs=1, league_average_gpct=0.769262, ppf=99.0, replacement_gpct=0.7342619, gwar=0.1699602, key_retro='watse101' WHERE id=1;"

# Example: Delete a database entry
delete:
	cargo run query "DELETE FROM Goose WHERE id=1;"

release:
	cargo build --release

all: format lint test extract transform_load create read update delete release
